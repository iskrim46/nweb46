#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "globals.h"

/* this is a child web server process, so we can exit on errors */
void web(int fd, int hit) {
    int j, file_fd, buflen;
    long i, ret, len;
    char *fstr;
    static char buffer[BUFSIZE + 1]; /* static so zero filled */

    ret = read(fd, buffer, BUFSIZE); /* read Web request in one go */
    if (ret == 0 || ret == -1)       /* read failure stop now */
        logger(FORBIDDEN, "failed to read browser request", "", fd);

    if (ret > 0 && ret < BUFSIZE) /* return code is valid chars */
        buffer[ret] = 0;            /* terminate the buffer */
    else
        buffer[0] = 0;
    for (i = 0; i < ret; i++) /* remove CF and LF characters */
        if (buffer[i] == '\r' || buffer[i] == '\n')
            buffer[i] = '*';

    logger(LOG, "request", buffer, hit);

    if (strncmp(buffer, "GET ", 4) != 0 || strncmp(buffer, "get ", 4) != 0)
        nweb_get(buffer, fd, hit);
    else
        logger(FORBIDDEN, "Only simple GET operation supported", buffer, fd);

    write(fd, buffer, strlen(buffer));

    /* send file in 8KB block - last block may be smaller */
    while ((ret = read(file_fd, buffer, BUFSIZE)) > 0)
        write(fd, buffer, ret);

    sleep(1); /* allow socket to drain before signalling the socket is closed */
    close(fd);
    exit(1);
}


/*
 * TODO: update help message
 */
void usage()
{
    printf(
        "hint: nweb Port-Number Top-Directory\t\tversion %d\n\n"
        "\tnweb is a small and very safe mini web server\n"
        "\tnweb only servers out file/web pages with extensions named below\n"
        "\t and only from the named directory or its sub-directories.\n"
        "\tThere is no fancy features = safe and secure.\n\n"
        "\tExample: nweb 8181 /home/nwebdir &\n\n"
        "\tOnly Supports:",
        VERSION);
    for (int i = 0; extensions[i].ext != 0; i++)
        printf(" %s", extensions[i].ext);

    printf(
        "\n\tNot Supported: URLs including \"..\", Java, Javascript, CGI\n"
        "\tNot Supported: directories / /etc /bin /lib /tmp /usr /dev /sbin \n"
        "\tNo warranty given or implied\n\tMykhailo Chernsyh iskrim@iskrim.xyz\n\tNigel Griffiths nag@uk.ibm.com\n");
    exit(0);
}

void setup_process()
{
    /* Become deamon + unstopable and no zombies children (= no wait()) */
    if (fork() != 0)
        exit(0);                    /* parent returns OK to shell */

    signal(SIGCLD, SIG_IGN); /* ignore child death */
    signal(SIGHUP, SIG_IGN); /* ignore terminal hangups */

    for (int i = 0; i < 32; i++)
        close(i); /* close open files */

    setpgrp();  /* break away from process group */

    return;
}

int setup_socket(char *port_str, int *listenfd, struct sockaddr_in *serv_addr)
{
    int port, socketfd;

    /* setup the network socket */
    if ((*listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        logger(ERROR, "system call", "socket", 0);

    port = atoi(port_str);
    if (port < 0 || port > 60000)
        logger(ERROR, "Invalid port number (try 1->60000)", port_str, 0);

    serv_addr->sin_family = AF_INET;
    serv_addr->sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr->sin_port = htons(port);

    if (bind(*listenfd, (struct sockaddr *)serv_addr, sizeof(*serv_addr)) < 0)
        logger(ERROR, "system call", "bind", 0);
    if (listen(*listenfd, 64) < 0)
        logger(ERROR, "system call", "listen", 0);

    return socketfd;

}

void connection_loop(int socketfd, int listenfd, struct sockaddr_in *cli_addr)
{
    socklen_t length;
    int pid;
    for (int hit = 1;; hit++) {
        length = sizeof(cli_addr);
        if ((socketfd = accept(listenfd, (struct sockaddr *)cli_addr, &length)) < 0)
            logger(ERROR, "system call", "accept", 0);
        if ((pid = fork()) < 0)
            logger(ERROR, "system call", "fork", 0);
        else
        {
            if (pid == 0) /* child */
            {
                close(listenfd);
                web(socketfd, hit); /* never returns */
            }
            else                /* parent */
                close(socketfd);
        }
    }
}

int main(int argc, char **argv) {
    int socketfd, listenfd;
    static struct sockaddr_in cli_addr;  /* static = initialised to zeros */
    static struct sockaddr_in serv_addr; /* static = initialised to zeros */

    if (argc < 3 || argc > 3 || !strcmp(argv[1], "-?"))
        usage();

    if (!strncmp(argv[2], "/", 2) || !strncmp(argv[2], "/etc", 5)    ||
        !strncmp(argv[2], "/bin", 5) || !strncmp(argv[2], "/lib", 5) ||
        !strncmp(argv[2], "/tmp", 5) || !strncmp(argv[2], "/usr", 5) ||
        !strncmp(argv[2], "/dev", 5) || !strncmp(argv[2], "/sbin", 6))
            FMT_ERR("ERROR: Bad top directory %s, see nweb -?\n", argv[2]);

    if (chdir(argv[2]) == -1)
        FMT_ERR("ERROR: Can't Change to directory %s\n", argv[2]);

    setup_process();

    logger(LOG, "nweb starting", argv[1], getpid());

    socketfd = setup_socket(argv[1], &listenfd, &serv_addr);

    connection_loop(socketfd, listenfd, &cli_addr);
}
