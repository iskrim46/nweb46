#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>

#include "globals.h"

/*concatinate formatted string to buffer*/
int fstrcat(char buffer[static BUFSIZE], const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);
    char tempbuffer[BUFSIZE];
    vsnprintf(tempbuffer, BUFSIZE - strlen(buffer), fmt, args); /*format temp buffer. max size is "free" chars in buffer*/
    va_end(args);

    strcat(buffer, tempbuffer);
    return strlen(buffer);
}

char *check_ext(char *buffer, int fd, int hit, int *success)
{

    int j, file_fd, buflen;
    long i, ret, len;
    char *fstr;
    
    buflen = strlen(buffer);
    fstr = (char *)0;
    *success = 1;
    for (int i = 0; extensions[i].ext != 0; i++)
    {
        len = strlen(extensions[i].ext);
        if (!strncmp(&buffer[buflen - len], extensions[i].ext, len))
        {
            fstr = extensions[i].filetype;
            break;
        }
    }

    if (fstr == 0)
    {
        logger(LOG, "file extension type not supported. Using text/code", buffer, fd);
        fstr = "text/plain";
        *success = 0;
    }

    return fstr;
}

void logger(int type, char *s1, char *s2, int socket_fd) {
    int fd;
    char logbuffer[BUFSIZE * 2];

    switch (type) {
    case ERROR:
        sprintf(logbuffer, "ERROR: %s:%s Errno=%d exiting pid=%d", s1, s2,
                errno, getpid());
        break;
    case FORBIDDEN:
        write(socket_fd,
              "HTTP/1.1 403 Forbidden\nContent-Length: 185\nConnection: "
              "close\nContent-Type: text/html\n\n<html><head>\n<title>403 "
              "Forbidden</title>\n</head><body>\n<h1>Forbidden</h1>\nThe "
              "requested URL, file type or operation is not allowed on this "
              "simple static file webserver.\n</body></html>\n",
              271);
        sprintf(logbuffer, "FORBIDDEN: %s:%s", s1, s2);
        break;
    case NOTFOUND:
        write(socket_fd,
              "HTTP/1.1 404 Not Found\nContent-Length: 136\nConnection: "
              "close\nContent-Type: text/html\n\n<html><head>\n<title>404 "
              "Not Found</title>\n</head><body>\n<h1>Not Found</h1>\nThe "
              "requested URL was not found on this server.\n</body></html>\n",
              224);
        sprintf(logbuffer, "NOT FOUND: %s:%s", s1, s2);
        break;
    case LOG:
        sprintf(logbuffer, " INFO: %s:%s:%d", s1, s2, socket_fd);
        break;
    }
    /* No checks here, nothing can be done with a failure anyway */
    if ((fd = open("nweb.log", O_CREAT | O_WRONLY | O_APPEND, 0644)) >= 0) {
        write(fd, logbuffer, strlen(logbuffer));
        write(fd, "\n", 1);
        close(fd);
    }
    if (type == ERROR || type == NOTFOUND || type == FORBIDDEN)
        exit(3);
}