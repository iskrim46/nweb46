#include "globals.h"

struct extension extensions[] = {
    {"gif",  "image/gif"},
    {"jpg",  "image/jpg"},
    {"jpeg", "image/jpeg"},
    {"png",  "image/png"},
    {"ico",  "image/ico"},
    {"zip",  "image/zip"},
    {"gz",   "image/gz"},
    {"tar",  "image/tar"},
    {"htm",  "text/html"},
    {"html", "text/html"},
    {"txt",  "text/plain"},
    {0, 0}
};