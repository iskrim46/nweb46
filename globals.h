#pragma once

#define VERSION 46
#define BUFSIZE 8096
#define ERROR 42
#define LOG 44
#define FORBIDDEN 403
#define NOTFOUND 404

#ifndef SIGCLD
#define SIGCLD SIGCHLD
#endif

#define FILESIZE(fd, var) { \
  var = (long)lseek(fd, (off_t)0, SEEK_END); \
  lseek(fd, (off_t)0, SEEK_SET);             \
}


#define FMT_ERR(fmt, args...) {\
                               fprintf(stderr, "\033[31m" fmt "\0336[37m\n", ##args); \
                               exit(-1); \
                              }

struct extension{
    char *ext;
    char *filetype;
} ;

extern struct extension extensions[];

/*
 * utils.c
 */

void logger(int type, char *s1, char *s2, int socket_fd);
char *check_ext(char *buffer, int fd, int hit, int *success);
int fstrcat(char buffer[static BUFSIZE], const char *fmt, ...);


/*
 * nweb_get.c
 */

int nweb_get(char buffer[static BUFSIZE], int fd, int hit);