#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "globals.h"

int nweb_get(char buffer[static BUFSIZE], int fd, int hit)
{

    int j, file_fd, buflen, download;
    long i, ret, len;
    char *fstr, *filename, tempbuffer[2000];

    for (i = 4; i < BUFSIZE; i++)  /* null terminate after the second space to ignore extra stuff */
    {
        if (buffer[i] == ' ') /* string is "GET URL " +lots of other stuff */
        {
            buffer[i] = 0;
            break;
        }
    }

    for (j = 0; j < i - 1; j++) /* check for illegal parent directory use .. */
        if (buffer[j] == '.' && buffer[j + 1] == '.')
            logger(FORBIDDEN, "Parent directory (..) path names not supported", buffer, fd);

    if (!strncmp(&buffer[0], "GET /\0", 6) || !strncmp(&buffer[0], "get /\0", 6)) /* convert no filename to index file */
        strcpy(buffer, "GET /index.html");

    fstr = check_ext(buffer, fd, hit, &download);
    download = !download;

    filename = malloc(strlen(&buffer[5]) * sizeof(char));  /*copy filename to new string*/
    strcpy(filename, &buffer[5]);

    if ((file_fd = open(filename, O_RDONLY)) == -1) /* open the file for reading */
        logger(NOTFOUND, "failed to open file", &buffer[5], fd);

    logger(LOG, "SEND", &buffer[5], hit);

    
    FILESIZE(file_fd, len);


    sprintf(buffer,
            "HTTP/1.1 200 OK\n"
            "Server: nweb/%d.0\n"
            "Content-Length: %ld\n"
            "Connection: close\n",
            VERSION, len); /* Header + a blank line */

    if(download)
        fstrcat(buffer, "Content-Disposition: attachment; filename=%s\n", filename);

    fstrcat(buffer, "Content-Type: %s\n\n", fstr);

    logger(LOG, "Header", buffer, hit);

    return 1;
}